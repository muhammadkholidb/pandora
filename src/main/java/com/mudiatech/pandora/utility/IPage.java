package com.mudiatech.pandora.utility;

public interface IPage {

    String templateName();
}
